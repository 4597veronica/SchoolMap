## Estudio inicial

Nuestro proyecto consistirá en una página web para el colegio. Esta página web, nos servirá para guiar a los visitantes por la escuela. El objetivo es que esta pagina pueda  verse bien tanto en móviles como en ordenadores. 

## Anàlisi del sistema

Para empezar tenemos un menú donde podemos escoger el lugar al que queremos ir. Cuando pulsemos en el, sobre el mapa aparecerán unas flechas que nos guiarán desde el edificio central hasta el lugar escogido. 

## Herramientas y conceptos

### Herramientas

Las herramientas que hemos utilizado son:
 
#### Gimp

Este programa lo hemos utilizado para hacer las flechas y los primeros mapas. Además de quitarle el fondo blanco a algunas imagnes.

#### Brackets

Este programa lo hemos utilizado para escribir el código HTML, javascript css y xml.

#### Geany

Este programa lo hemos utilizado para escribir el código HTML, javascript css y xml

#### My sweet home   

Este programa lo hemos utilizado para hacer los últimos planos.

### Conceptos

#### Responsive
Es una técnica para diseñar las páginas web. Nos ayuda a que se puedan ver tanto en móviles como en pc. Además, cuando estemos delante de un PC y cambiemos el tamaño del navegador tambíen se adapte. Hay dos tipos de responsive, dependiendo del tamaño inicial en que nos basamos para crear la página web. 
Si nos basamos en el tamaño más pequeño, como por ejemplo un móvil se llama mobile first. En el diseñamos primero el css para la página web de un móvil y después vamos añadiendo o cambiando los elementos de la página, adaptandolos al tamaño. Si nos basamos en el tamaño más grande, como por ejemplo un PC. Creamos el css de la misma manera, pero cambiando o añadiendo elementos según vaya empequeñeciendo la pantalla. 

Para diferenciar que elementos del css  corresponden al tamaño indicado escribimos esta línea.

	@media only screen and (max-width: 992px) {}

Esta línea nos sirve si estamos haciendo un PC first, donde le estamos indicando que este elemento se verá, en una pantalla menor de 992px.
Si por el contrario nos ponemos a hacer un mobile first cambiaremos esta línea por esta otra.

	@media only screen and (min-width: 992px) {}
	
Aquí le estamos indicando que los elementos se mostrarán a partir de una pantalla mayor que 992px. 
El estilo que vayamos a escoger es muy importante, ya que se van cargando los elementos hasta que llega al tamaño de pantalla del dispositivo.


#### JSON

JSON (JavaScript Object Notation), es una sintaxis para almacenar e intercambiar datos en formato texto.

Al intercambiar datos entre un navegador y un servidor, los datos sólo pueden ser texto.
JSON es texto, y podemos convertir cualquier objeto JavaScript en JSON y enviar JSON al servidor.
También podemos convertir cualquier JSON recibido del servidor en objetos JavaScript.
De esta forma podemos trabajar con los datos como objetos de JavaScript, sin análisis ni traducciones complicadas.


## Diseño del sistema

Hemos realizado una página web con los lenguajes de html, css (hojas de estilo) y js (javascript) de forma que nos muestre diferentes rutas con botones de siguiente, siguiente.


## Desenvolvimiento

Para crear la página web, nos hemos dividido el trabajo. A continuación cada uno explicara que ha hecho, con sus propias palabras.

### Veronica:

### Flechas
Para animar las flechas he utilizado el programa gimp. En un principio habiamos escogido el tipo de flecha, siguiente.

![izquierda2](/home/veronica/Escritorio/mapa/images/flecha.png)
                         
Este tipo, no nos ha convenido porque para animarla o hacíamos una animación muy rápida o necesitábamos muchas capas. 
Después encontré otro tipo de flecha, que se veía más adecuada y podiamos animarla sin problemas
 
![izquierda](/home/veronica/Escritorio/mapa/images/izquierda.gif)
                         


### Mapas
El primer problema que hemos tenido ha sido intentar saber como hacer los mapas. En un principio teníamos unos mapas proporcionados por la escuela. Pero estos no nos acababa de convencer, por los colores. 

![gradomedioinf](/home/veronica/Escritorio/gmedio.png)

Después basándome en estos hice unos mapas con gimp, pero estos no nos gustaron ya que nos parecieron muy sencillos y básicos.

![mapa-fusta](/home/veronica/Escritorio/mapa_fusta-planta-baja.jpg)


Finalmente descubrí un programa para dibujar edificios, my sweet home 3D. Es con este programa con el que hemos terminado haciendo los mapas. 

![cen](/home/veronica/Escritorio/edificio-central.png)

### Javascript
El java script lo he utilizado para ocultar y mostrar las flechas. Además de para cambiar los idiomas. Esto último ha sido provisionalmente, porque esto se hará llamando al json.

Función para cambiar el idioma

    function ingles() {
        document.getElementById("idiomas").innerHTML = "Languages";
        document.getElementById("menu").innerHTML = "Menu";
        document.getElementById("sala").innerHTML = "Audiovisual room";
        document.getElementById("tallers").innerHTML = "Workrooms";
        document.getElementById("departaments").innerHTML = "Departament";
    }

Función para ocultar y mostrar las flechas (ruta biblioteca)

            /* Para mostrar las flechas*/

    /*Estas pertenecen a las flechas del edificio central */
    //Flechas de la biblioteca
    function planta1() {
        'use strict';
        var x = document.getElementById("biblioteca_gif");
        //Flechas que van hacia arriba
        if (x.style.display === "block") {
            x.style.display = "none";
        } else {
            x.style.display = "block";
        }
        var y = document.getElementById("siguiente");
        //Flechas que van hacia arriba
        if (y.style.display === "block") {
            y.style.display = "none";
        } else {
            y.style.display = "block";
        }
    }
    //Esta funcion te enseña un boton para volver al mapa inicial
    function anterior(){
    	 'use strict';
    	var anterior = document.getElementById("anterior");
        //Flechas que van hacia arriba
        if (anterior.style.display === "block") {
            anterior.style.display = "none";
        } else {
            anterior.style.display = "block";
        }
    }
            
    //Esta funcion te enseña un link para ver la biblioteca
    function ver_biblioteca(){
    	 'use strict';
    	var foto = document.getElementById("foto_biblio");
        //Flechas que van hacia arriba
        if (foto.style.display === "block") {
            foto.style.display = "none";
        } else {
            foto.style.display = "block";
        }
    }

Función para cambiar de mapas

        function p1(){
            document.getElementById('mapa').src = "./images/central_p1.png";
        }
        function p0(){
             document.getElementById('mapa').src = "./images/central_p0.png";
        }
        function patio(){
            document.getElementById('mapa').src = "./images/patio.png";
        }
        function superior(){
            document.getElementById('mapa').src = "./images/superior.png";
        }
        function medio(){
            document.getElementById('mapa').src = "./images/medio.png";
        }

También  he aprovechado esta función para hacer otra que muestre la foto del sitio, al llegar.

    function foto_biblio(){
    	 document.getElementById('mapa').src = "./images/central_p1.png";
    }
    function foto_direccion(){
    	 document.getElementById('mapa').src = "./images/central_p1.png";
    }

Además encontré un script y un css específico para poder hacer el menú adaptable.

    /*---------------------Menu--------------------------------*/
    $(document).ready(main);
     
    var contador = 1;
     
    function main () {
    	$('.menu_bar').click(function(){
    		if (contador == 1) {
    			$('nav').animate({
    				left: '0'
    			});
    			contador = 0;
    		} else {
    			contador = 1;
    			$('nav').animate({
    				left: '-100%'
    			});
    		}
    	});
     
    	// Mostramos y ocultamos submenus
    	$('.submenu').click(function(){
    		$(this).children('.children').slideToggle();
    	});
    }
        
    @media screen and (max-width: 800px) {
         .menu_bar {
            display:block;
            width:100%;
            position: fixed;
            top:0;
            left: 0;
            background: #5f6a6a ;
        }
            
        .menu_bar .bt-menu {
            display: block;
            padding: 2%;
            overflow: hidden;
            font-size: 25px;
            font-weight: bold;
            text-decoration: none;
            
        }
            
        .menu_bar span {
            float: right;
            font-size: 40px;
            
        }
            
        header nav {
            width: 80%;
            height: calc(100% - 80px);
            position: fixed;
            right:100%;
            overflow: scroll;
            margin:0%;
            margin-top:2%;
            padding:0;
        }
            
        header nav ul li {
            display: block;
            border-bottom:1px solid rgba(255,255,255,.5);
            
        }
            
        header nav ul li a {
            display: block;
           
        }
            
        header nav ul li:hover .children {
            display: none;
        }
            
        header nav ul li .children {
            width: 100%;
            position: relative;
            
        }
            
        header nav ul li .children li a {
            margin-left:3%;
        }
            
        header nav ul li .caret {
            float: right;
        }
    }


#### Responsive

En un principio el responsive, era para que los mapas y las flechas se adaptaran a todo tipo de pantallas y tamaños. Sin embargo solo he logrado que se adapte a lo ancho. El css de divide en dos partes principales. La primera parte es para una ventana con el tamaño de un ordenador, con el menú siguiente.
![menu](/home/veronica/Escritorio/menu1.png)

La segunda parte es para ventanas más pequeñas, como una tablet o móvil. Para este tamaño he hecho este menú.

![menu](/home/veronica/Escritorio/menu2.png)
Después he ido cambiando los elementos del HTML según el tamaño. Estos han sido los diferentes tamaños en los que he hecho cambios.

	@media only screen and (max-width: 1010px) {}
	@media only screen and (max-width: 1000px) {}
	@media only screen and (max-width: 950px) {}
	@media only screen and (min-width:801px) and (max-width: 899px) {}
	@media only screen and (max-width: 800px) {}
	@media only screen and (max-width: 768px) {}
	@media only screen and (max-width: 600px) {}

A la hora de hacer el responsive he tenido varios problemas. El primero es que me costaba aclararme a que tamaño de pantalla corresponden los pixeles. Esto lo solucione rápido, creando un archivo llamado prueba color. Donde el color de fondo cambiaba según los pixeles.   Más tarde, utilice esta técnica para ir modificando el css con el mapa. 


	/*----------------Version Navajowhite--------------------*/
	@media only screen and (max-width: 1010px){
	    body{
		background-color: navajowhite
	    }
	}

El segundo problema es que si abríamos el archivo html en el móvil, la pantalla no se adaptaba. Era como si no reconociera el dispositivo. 
Finalmente, pude solucionar el problema, poniendo esta línea en el head. Me costó encontrarla, porque todos los tutoriales e información que encontraba, solo explicaban, cómo funciona el css.

	<meta name="viewport" content="width=device-width" >
	
 Después añadimos más a esta línea, para limitar el zoom que se puede hacer. 
	 
	<meta name="viewport" content="width=device-width, user-scalable=no initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" >


### Alex:

### JSON
Para que salgan los nombres de las clases/aulas/departamentos y el texto según la ruta deseada.

JSON (JavaScript Object Notation), es una sintaxis para almacenar e intercambiar datos en formato texto.

Al intercambiar datos entre un navegador y un servidor, los datos sólo pueden ser texto.
JSON es texto, y podemos convertir cualquier objeto JavaScript en JSON y enviar JSON al servidor.
También podemos convertir cualquier JSON recibido del servidor en objetos JavaScript.
De esta forma podemos trabajar con los datos como objetos de JavaScript, sin análisis ni traducciones complicadas.



Aquí una muestra del javascript llamando al json. 


![menu](/home/veronica/Escritorio/mapa.png)

También a través de json hemos conseguido cambiar de idioma las opciones del menú a inglés, castellano y catalán. Todo gracias a una función puesta en el menú para cuando quieres cambiar de idioma.

Aquí una muestra de la función del cambio de idioma y sus respectivos cambios en el menú.

![](/home/veronica/Escritorio/mapa2.png) 


###PROBLEMAS GENERALES

**· Problema con el xml: **
La intención inicial del proyecto era llamar a cada columna del xml desde javascript, pero no se dió y junto a todo el equipo decidimos pasarlo a json, desde json llamamos a las diferentes clases, departamentos,... según la ruta tomada. 



**· Problema con el menú: **
Con el tema del menú hemos tenido varios problemas. Uno de ellos ha sido el que no se podía mostrar en dispositivo móvil. Finalmente encontramos un menú ya hecho y diseñado para el responsive. La primera idea era hacer el menú, con la parte desplegable transparente. Esto también nos costó encontrarlo. Aunque finalmente no lo hemos utilizado.

** Cambios de diseño:**
Hemos tenido que hacer varios cambios de diseño general en cuanto a los mapas de fondo (al principio muy básicos, después colores más vivos y finalmente un mix entre los dos.), también ha habido un importante cambio de diseño de los botones y el menú en general hasta adaptar la página web a unos colores más grisáceos.

** Mala planificación: **

	· 1 Mapa/semana (problemas varios[xml,css]): En la planificación en general teníamos pensado ir a mapa por semana pero como en la mayoría de proyectos, siempre suceden contratiempos.

	· Rediseño de página web: Hemos rediseñado la página web ya que hemos tenido que cambiar diseño y hemos tenido que correr a última hora para entregar varios mapas.

	· Conceptos no afianzados: Hemos tocado varios temas en el proyecto los cuáles no teníamos correctamente afianzados y hemos ido cogiendo a medida que transcurría dicho período. Un buen ejemplo el caso del responsive de css o el json. 


	· Fotos no realizadas: Debido a los anteriores retrasos no se ha podido hacer todas las fotografías deseadas para tener completo el proyecto.

	· Javascript idiomas provisional: También para el tema del idioma se hizo un javascript aparte con los cambios de idioma.

	· Rutas inacabadas: Al igual que las fotografías, debido a suso dichos retrasos no hemos podido acabar todas las rutas que queríamos.
	
##Implantación
Para implantar todo lo del proyecto hemos tenido que montar un servidor web en el localhost. Para ello hemos tenido que instalar el Apache Server (ya previamente instalado en clase) y hemos tocado el archivo de configuración httpd.conf para que nos funcione a la perfección nuestra web.

##Mantenimiento
Nuestro proyecto no necesita mucho mantenimiento. Como toda página web necesita tener un servidor web activo y una IP fija  para poder acceder a ella remotamente. 
Conclusiones


###Valoraciones:
	-Alex: Con un poco más de tiempo creo que se podía haber llegado mucho más allá en lo que el proyecto se refiere, es decir, fotos en todas las rutas, y todas las rutas acabadas; entre otras cosas. Como valoración general decir que me esperaba más dado a la planificación previa hecha, pero por otra parte dado a los innumerables problemas que hemos tenido creo que llegar hasta donde hemos llegado es en nota general positivo. Eso sí creo que el diseño en sí de la web está muy bien y como buen diseño merece ser bien reconocido al igual que el gran trabajo json.
	
	-Verónica:
		

##Bibliografía
Navegación páginas adelante, atrás.
https://www.w3schools.com/css/tryit.asp?filename=trycss_sprites_nav
Esconder flechas (extraído de aquí)
https://www.lawebdelprogramador.com/foros/JavaScript/1065330-ocultar-y-mostrar-imagenes-textos-y-formularios.html
Responsive
https://www.w3schools.com/css/css_rwd_intro.asp
https://www.w3schools.com/css/css_rwd_mediaqueries.asp
http://blog.es.mailify.com/envio-emailing/ocultar-elementos-version/






##Annexos

Este es el css que hemos utilizado para practicar con el css.

		@media only screen and (max-width: 1010px) {
			body{
				background-color: red;
			  }
		}
		@media only screen and (max-width: 1000px) {
			body{
				background-color: blue;
			  }
		}
		@media only screen and (max-width: 950px) {
			body{
				background-color: black;
			  }
		}
		@media only screen and (min-width:801px) and (max-width: 899px) {
			body{
				background-color: grey;
		 	}
		}
		@media only screen and (max-width: 800px) {
			body{
				background-color: yellow;
		 	}
		}
		@media only screen and (max-width: 768px) {
			body{
				background-color: pink;
		 	}
		}
		@media only screen and (max-width: 600px) {
			body{
				background-color: green;
		 	}
		}
Además para las flechas, hemos tenido que hacer otro javascript para las flechas

![](/home/veronica/Escritorio/flechas.png) 
